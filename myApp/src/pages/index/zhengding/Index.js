import Taro, { Component } from '@tarojs/taro'
import { View, Input , Video } from '@tarojs/components'
import './Indexs.css'
import indexbt from './img/indexbt.png'
import imglogo from './img/logo.png'

class Indexs extends Component {
  constructor(props) {
    super(props);
this.state={cataname:[]}
  }

toints(id){
    const idd=id.currentTarget.dataset.id
    const linn=id.currentTarget.dataset.lin
    const tit=id.currentTarget.dataset.links
    if(linn.indexOf("https://") != -1){
       Taro.navigateTo({
        url: '/pages/index/zhengding/Link?lin='+linn+'&title='+tit
      })
      
   }else{
     if(idd==60){
        Taro.switchTab({
          url: linn
        })
    }else{
        Taro.navigateTo({
          url: linn+'?id='+idd+'&title='+tit
        })
    }
        
	}
  }

     inf(){
     
       Taro.request({
      url:'https://mp.wangshouxin.cn/api/links',
      data: {
        id:34
      },
      header: {
        'content-type': 'application/json'
      }
    })
      .then(res =>this.setState({
        cataname:res.data
      })
      )
     }
     componentDidMount() {
       this.inf()
      }
    
  render() {
       
    return (
    <View className='indexq'>
      <View className='header'>
       <Image src="http://qiniu.wangshouxin.cn/1.png"  mode="widthFix" alt='' className='img-fluid head' />
       
       <Text className='indexp'>2019.4.26-5.1</Text>
       <Image src={indexbt} mode="widthFix"  className='img-fluid head1'/>
       <View className='navul'>
      { this.state.cataname.map((art)=>
       <View style="font-size:18px;" data-id={art.linkid} data-lin={art.url} data-links={art.title} onClick={this.toints}><View className='triangle'></View>{art.title}</View>
       )
       }
       </View>
       <View className='logo'>
       <Image src={imglogo} className='head2' />
       </View>
        <View className='inrow'>
       <View className='one'><View className='incolo'></View></View>
       <View className='one'><View className='incolt'></View></View>
       <View className='two'><View className='incole'></View></View>
       <View style="clear:both;"></View>
       </View>
      <Text className='title'>中国·石家庄(正定)</Text>
      
      
       
       <View className='vide'>
      <Video
          src='http://qiniu.wangshouxin.cn/zhengbohui_shipin.mp4'
          autoplay={false}
          poster='http://qiniu.wangshouxin.cn/shipin.jpg'
          initialTime='0'
          id='video'
          loop={false}
          muted={false}
        />
       </View>
       <Image src="http://qiniu.wangshouxin.cn/2-.png"  mode="widthFix" className='img-fluid footimg'/>
       </View>
       <View className='footer'>
       
       </View>
     </View>
    );
  }
}


export default Indexs;
