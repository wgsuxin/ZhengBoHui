import Taro, { Component } from '@tarojs/taro'
import { View, Input } from '@tarojs/components'
import'./Traffic.css'

class Traffic extends Component{
    config: Config = {
      navigationBarTitleText: '交通导航'
    }
   
    constructor(props) {
           super(props);
           this.state={cataname:[]}
           }
           
    
          fg(){
                     var tt=this
                     Taro.request({
                         method: 'POST',
                    url: 'https://mp.wangshouxin.cn/api/navigation/index',
                    data: {
                      id:this.$router.params.id
                    },
                    header: {
                      'content-type': 'application/json'
                    },
                    success(res){
                    tt.setState({cataname:res.data.data})
                    
                  }
                   })
                   }
                   
                   componentDidMount() {
                     this.fg()
                    }
                  
            getLocation (e) {
             wx.getLocation({
               type: 'wgs84',
               success: function (res) {
                 wx.openLocation({//​使用微信内置地图查看位置。
                   latitude: e.currentTarget.dataset.lat-0,//要去的纬度-地址
                   longitude: e.currentTarget.dataset.lng-0,//要去的经度-地址
                   name: e.currentTarget.dataset.name,
                   address: e.currentTarget.dataset.name
                 })
               }
             })
           }
        render() {
          
         return (
        <View className="traff">
         { this.state.cataname.map((art)=>
          <View mode="widthFix" onClick={this.getLocation} data-lat={art.my_latitude} data-lng={art.my_longitude} data-name={art.name}><Image src={art.thumb?('https://mp.wangshouxin.cn/uploads/'+art.thumb.replace(/\\/g,"\/")):infor1}  mode="widthFix" className="img-fluid traff_img"/></View>
        )
        }
      
         </View>
         )
         
        
         }
           
           
}
export default Traffic ;