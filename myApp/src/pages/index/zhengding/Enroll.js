import Taro, { Component } from '@tarojs/taro'
import { View, Input , Text , Form } from '@tarojs/components'
import './Enroll.css';
import cha from './img/cha.png'
import fang1 from './img/fang1.png';
class Enroll extends  Component{
   config: Config = {
    navigationBarTitleText: '预约报名'
  }
constructor(props){
            super(props);
           this.state = {phone:'',ming:'',email:'',name:'',address:'',ping:'',src:'',cha:'',image:''};
            this.sub = this.sub.bind(this);
            this.ch = this.ch.bind(this); 
            this.ch1 = this.ch1.bind(this);
             this.ch2 = this.ch2.bind(this);
             this.ch3 = this.ch3.bind(this);
             this.ch4 = this.ch4.bind(this);
             this.ch5 = this.ch5.bind(this);
    }
    
    ch = (event) => {
    this.setState({
      phone : event.target.value,
    })
  };
  ch1 = (event) => {
    this.setState({
      ming : event.target.value,
    })
  };
  ch2 = (event) => {
    this.setState({
      email : event.target.value,
    })
  };
   ch3 = (event) => {
    this.setState({
      name : event.target.value,
    })
  };
  ch4 = (event) => {
    this.setState({
      address : event.target.value,
    })
  };
  ch5 = (event) => {
    this.setState({
      ping: event.target.value,
    })
  };
  ch9 = (event) => {
    this.setState({
      image: event.target.value,
    })
  };
  sub = () => {
    const {phone} = this.state;
    const {ming} = this.state;
    const {email} = this.state;
    const {name} = this.state;
    const {address} = this.state;
    const {ping} = this.state;
    var reg=/^[\u0391-\uFFE5]+$/; 
    var myreg = /^1[345789]\d{9}$/; 
 var mreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
 
     if(ming==""){
     Taro.showModal({title:'请输入正确商家名称'})
    }else if(ping==""){
      Taro.showModal({title:'请输入正确商主营商品'})
    }else if(name==""){
      Taro.showModal({title:'请输入正确联系人姓名'})
    }else if(phone.length>11 || phone==0 || !myreg.test(phone)){
       Taro.showModal({title:'输入有效联系人电话'})
    }else if(email=="" || !mreg.test(email)){
      Taro.showModal({title:'请输入正确联系人邮箱'})
    }else if(address==""){
      Taro.showModal({title:'请输入正确商家地址'})
      
    }
    else{
      var inp=this
       Taro.request({  
              url: 'https://mp.wangshouxin.cn/api/enlist',  
              header: {  
                "Content-Type": "application/x-www-form-urlencoded"  
              },
              method: "POST",
              data: {phone:this.state.phone,sellername:this.state.ming,email:this.state.email,name:this.state.name,site:this.state.address,goods:this.state.ping,image:this.state.image},
           success(res) {
                    Taro.showModal({
                      title:'提交成功'
                    })
             inp.setState({
              phone:'',ming:'',email:'',name:'',address:'',ping:'',image:'',src:'',cha:''
             })  
            
      },
      })
  }

}
 
 chooseimage() {
   var vv=this
    Taro.chooseImage({
  count: 1,
  sizeType: ['original', 'compressed'],
  sourceType: ['album', 'camera'],
   success(res) {
    const tempFilePaths = res.tempFilePaths
   vv.setState({src:res.tempFilePaths,cha:cha})
    console.log('2343');
   Taro.uploadFile({ 
          url: 'http://mp.wangshouxin.cn/api/enlist/img',  
          filePath:tempFilePaths[0], 
           name: 'image',
          header: { "Content-Type": "multipart/form-data" },
      formData:{},
          success(res) { 
           var data =JSON.parse(res.data)
           vv.setState({image:data})
              
          }
           }); 
      
  }
})
  }

 
 
 
  deleteImg(){
    this.setState({src:'',cha:''})
  }
 
            render () {
                var {value} = this.state;
               
              return (
              
              <View className='enroll'>
              <Image className='enimg' mode="widthFix" src="http://qiniu.wangshouxin.cn/enroll_01.gif" />
              <Image className='enimg' mode="widthFix" src="http://qiniu.wangshouxin.cn/enroll_02.gif" />
              <Image className='enimg' mode="widthFix" src="http://qiniu.wangshouxin.cn/enroll_03.gif"/>
             <Form className="ffo example">
        <Input type='text' name='mingc' placeholder='商家名称' value={this.state.ming} onChange={this.ch1}/>
        
      <Input type='text' name='ping' placeholder='商主营商品' value={this.state.ping} onChange={this.ch5}/>
              <Input type='text' name='usname' placeholder='联系人姓名' value={this.state.name} onChange={this.ch3}/>
              <Input type='text' name='phone' placeholder='联系人电话' value={this.state.phone} onChange={this.ch}/>
              <Input type='text' name='email' placeholder='联系人邮箱' value={this.state.email} onChange={this.ch2}/>
              <Input type='text' name='address' placeholder='商家地址' value={this.state.address} onChange={this.ch4}/>
               <Input className='tut' type='text' name='address' value={this.state.image}/>
              
               <View className='huoqu' onClick={this.chooseimage}>
                        <Image className='add' src={fang1}/>
                        <Text className="zhi">营业执照图片</Text>
                        
                        </View>
                        <View className='yz'>
              <Image className='mn' style="width:85px;height:75px" src={this.state.src}/>
              <Image style="width:20px;height:20px" className="cha" src={this.state.cha} 
              onClick={this.deleteImg}/></View>
              
              
            </Form>
            
           
            <Button className="bbk" type='submit' onClick={this.sub}>提交</Button>
              
             
              </View>
              );
              }
}

  
export default Enroll;