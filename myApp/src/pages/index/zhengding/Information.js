import Taro, { Component, Config } from '@tarojs/taro';
import { View, Text,Image } from '@tarojs/components';
import './Indexs.css';
import infor1 from './img/infor1.jpg';

class Information extends Component {
   
    showtitle(tit){
      Taro.setNavigationBarTitle
      ({
          title: tit
     })
    }
   
   
  constructor(props) {
    super(props);
   this.state={cataname:[],link:'',page:1,style:true},
   this.xiang=this.xiang.bind(this)
 
  }
 componentWillMount () {
  
 const tnt=this.$router.params.title;
 this.showtitle(tnt)
  }
 
 
 xiang(idd){
     const lin=idd.currentTarget.dataset.link
    
     if(lin!=''){
       Taro.navigateTo({
         url: '/pages/index/zhengding/Link?lin='+lin
       })
     }else{
    const dd=idd.currentTarget.dataset.id
    
   		Taro.navigateTo({
   		  url: '/pages/index/zhengding/Details?id='+dd
   		})
       }
   	}

     onPullDownRefresh(){
              var tt=this
             tt.setState({page:1})
            Taro.request({
                method: 'POST',
           url: 'https://mp.wangshouxin.cn/api/pen?pages=10',
           data: {
             id:this.$router.params.id
           },
           header: {
             'content-type': 'application/json'
           },
           success(res){
           tt.setState({cataname:res.data.data})
           Taro.stopPullDownRefresh();
         }
          })
         tt.setState({style:true})
          }
          
          onReachBottom(){
            var that = this;
            if(that.state.style){
          var page=that.state.page
            page =page + 1
          that.setState({page:page})
            Taro.request({
                   method: 'POST',
              url: 'https://mp.wangshouxin.cn/api/pen?page='+page+'&pages=10',
              data: {
                id:that.$router.params.id
              },
              header: {
                'content-type': 'application/json'
              },
              success(res){
                if(res.data.data.length!=0){
                var catename=that.state.cataname
                for (var i = 0; i < res.data.data.length; i++) {
                catename.push(res.data.data[i]);
        }
                
              that.setState({cataname:that.state.cataname})
              
            }else{
              that.setState({style:false})
            }
           
            }
             })
           }
          }
          
         
          componentDidMount() {
            this.onPullDownRefresh();
            
           }
         
  render() {
    
    const list = this.state.cataname.map((art)=>{
      return(
      <View className='inform'>
    <View classname='bi'>
      <View className='inforone' data-id={art.id} data-link={art.link} onClick={this.xiang}>
      <View className='inf-imgs'>
      <Image src={art.thumb?('https://mp.wangshouxin.cn/uploads/'+art.thumb.replace(/\\/g,"\/")):infor1} mode="widthFix" className='nn' />
      </View>
      <Text className='tt'>{art.title}</Text>
      </View>
        
      </View>
     </View>  )})
     
    return (
    <View>
  <View className="info-list">{list}</View>
  <View className='sho'>{style?'正在加载....':'———已经到底了哦!———'}</View>
  </View>
  
  );
  }
}

export default Information;