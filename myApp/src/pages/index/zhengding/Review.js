import Taro, { Component } from '@tarojs/taro'
import { View, Input , Text , Form } from '@tarojs/components'
import WxParse from '../../../wxParse/wxParse'
import './Review.css';
class Review extends Component {
  
  showtitle(tit){
    Taro.setNavigationBarTitle
    ({
        title: tit
   })
  }
  constructor(props) {
    super(props);
       this.state={thumb:'',title:'',time:'',cont:'',article:''}
  }
         fg(){
          var tt=this
          Taro.request({
              method: 'POST',
         url: 'https://mp.wangshouxin.cn/api/pen/content',
         data: {
           id:this.$router.params.id
         },
         header: {
           'content-type': 'application/json'
         },
         success(res){
                const content = res.data.content.replace(/\/ueditor\/php\/upload\/image\//g, 'https://mp.wangshouxin.cn/ueditor/php/upload/image/')
            const dd=WxParse.wxParse('article', 'html', content,tt, 5);
          tt.setState({
              title:res.data.title,time:res.data.time,cont:res.data.click,           thumb:res.data.thumb?('https://mp.wangshouxin.cn/uploads/'+res.data.thumb.replace(/\\/g,"\/")):'',})
      
       }
        })
        }
        
        componentDidMount() {
          this.fg()
         }
        componentWillMount () {
        const tnt=this.$router.params.title;
        this.showtitle(tnt)
         }
        
         formatTime(e) {
            var e = new Date(e*1000)
            var n = e.getFullYear(), r = e.getMonth() + 1, o = e.getDate(), u = e.getHours(), i = e.getMinutes(), a = e.getSeconds();
            return n+'年'+r+'月'+o+'日'+u+'时'+i+'分';
        }

  render() {
      
    return (
    <View className='Review'>
    
     <View className='details'>
        <View className='deta'>
       <View className='detail'>{this.state.title}</View>
      <Text className='despan'>{this.formatTime(this.state.time)}</Text><Text className='despan1'>阅读:{this.state.cont}</Text>
       </View>
       <View className='article'>
        {!!this.state.thumb && <View><Image src={this.state.thumb} mode="widthFix" /></View>}
      <import src='../../../wxParse/wxParse.wxml' />
      <template is="wxParse" data="{{wxParseData:article.nodes}}"/>
       </View>
    </View>
     </View>
    );
  }
}

export default Review;
