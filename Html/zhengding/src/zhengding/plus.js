import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Enroll.css';
import { Icon, Modal } from 'antd';
import PropTypes from 'prop-types';
import {  Button,Form ,Input} from 'reactstrap';
 
 class Plus extends Component{
      constructor(props) {
             super(props);
             this.state = {
      imgFile: null,
      imgSrc: this.props.imgSrc||"",
      renderState: this.props.renderState||"init",
      isPreview: false,
      disabled:this.props.disabled
   }
   }
        static defaultProps = {
    imgTitle: "",
    height: "100px",
    renderState: "init",
    titleClass: "",
    accept:"*"
  }

  static propTypes = {
    imgTitle: PropTypes.string,
    renderState: PropTypes.oneOf(['init', 'upload']),
    imgSrc: PropTypes.string,
    titleClass: PropTypes.string,
    accept:PropTypes.string
  };

  componentWillReceiveProps(nextProps){
    if(this.props.imgSrc != nextProps.imgSrc){
      this.setState({imgSrc:nextProps.imgSrc})
    }
    if(this.props.renderState != nextProps.renderState){
      this.setState({renderState:nextProps.renderState})
    }
    if(this.props.disabled != nextProps.disabled){
      this.setState({disabled:nextProps.disabled})
    }  

  }

 
 

  renderUploadq() {
    const imgSrc = (this.state.imgSrc)
    const imgBox = this.state.disabled ? "img-box-preview-hide" : "img-box-preview-show"
    return (
      <div className={imgBox} style={{ height: this.props.height }}>
      <img src={imgSrc} className="img-wh" />
        <div className="img-preview">
         
          <span type="delete" className="img-operate" onClick={this.deleteImg.bind(this)}>删除</span>
        </div>
      </div>
    )
  }
 renderInit() {
    return (
      <div className="img-box" style={{ height: this.props.height }}>
        <input type="file" className="img-file" onChange={this.imgChange.bind(this)} accept={this.props.accept} />
        <div className={"img-add " + this.props.titleClass}>
          <Icon type="plus" className="img-add-icon" />
          <div>{this.props.imgTitle}</div>
        </div>
      </div>
    )
  }

  renderImg() {
    if (this.state.renderState === "init") {
        return this.renderInit()
    }else if (this.state.renderState === "upload") {
     return this.renderUploadq()
    }
  }

  imgChange(event) {
    this.setState({ imgFile: event.target.files[0], renderState: "upload" }, () => {
      this.previewImg()
    })
  }

  previewImg() {
    const that = this;
    const file = this.state.imgFile;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function (e) {
      that.setState({ renderState: "upload", imgSrc: this.result })
    }
  }

  original() {
    this.setState({ isPreview: true })
  }

  deleteImg() {
    this.setState({ renderState: "init", imgFile: null, isPreview: false, imgSrc: "" })
  }

  resetImgSrc(){
    this.setState({ renderState: this.props.renderState, imgFile: null, imgSrc: this.props.imgSrc })
  }

  getImgFile() {
      return {file:this.state.imgFile,src:this.state.imgSrc}
  }

  cancelModal() {
    this.setState({ isPreview: false })
  }

             render () {
               
               
               return (
               
              <div>
        {this.renderImg()}
        <Modal visible={this.state.isPreview} footer={null}
          onCancel={this.cancelModal.bind(this)}
          width="auto" wrapClassName="img-center"
          style={{ display: "inline-block" }}
        >
          <img src={this.state.imgSrc} className="preview-all" />
        </Modal>
      </div>

               );
               }
 }
 export default Plus;