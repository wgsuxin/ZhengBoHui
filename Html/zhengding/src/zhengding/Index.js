import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Index.css';
import Bottom from './Bottom';
import Review from './Review';
import Information from './Information';
import Stand from './Stand';
import Routerz from './Routerz';
import Introduce from './Introduce';
import Activity from './Activity';
import Traffic from './Traffic';
import { Container, Row, Col } from 'reactstrap';
class Index extends Component {
  constructor(props) {
    super(props);

  }


  render() {
    return (
    <div className="index">
      <div className="header">
      <img src={require('./img/index.png')} alt="" />
      <div className="indexart">
      <p className="indexp">2019.4.26-5.1</p>
      <img src={require('./img/indexbt.png')}/>
      <ul className="navul">
      <li><div className="triangle"></div><a href="./Introduce">本届介绍</a></li>
      <li><div className="triangle"></div><a href="./Activity">活动简介</a></li>
      <li><div className="triangle"></div><a href="./Information">最新资讯</a></li>
      <li><div className="triangle"></div><a href="./Stand">展位信息</a></li>
      <li><div className="triangle"></div><a href="./Traffic">交通导航</a></li>
      <li><div className="triangle"></div><a href="./Review">历届回顾</a></li>
      </ul>
      <div className="logo">
      <img src={require('./img/logo.png')}/>
      </div>
       <Row className="inrow">
      <Col xs={4} className="one"><div className="incolo"></div></Col>
      <Col xs={4} className="one"><div className="incolt"></div></Col>
      <Col xs={4} className="two"><div className="incole"></div></Col>
      </Row>
     <p className="title">中国·石家庄(正定)</p>
     
     </div>
      </div>
      <div className="vide">
      <video controls></video>
      </div>
      <div className="bott">
      <img src={require('./img/2.png')} className="img-fluid"/>
      </div>
      
      
      <div className="footer">
      <Bottom/>
      </div>
     </div>
    );
  }
}

export default Index;
