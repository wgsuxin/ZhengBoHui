import React, { Component, PropTypes } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Enroll.css';
import Plus from './plus';
import enroll_01 from './img/enroll_01.gif';
import {  Button} from 'reactstrap';
import {Row, Col, Form ,Select,Input,Checkbox, Radio, Tooltip, Icon,DatePicker} from 'antd';
const FormItem = Form.Item;

class Enroll extends  React.Component{
     constructor(props) {
            super(props);
            
    }
            render () {
                const { getFieldDecorator } = this.props.form;
              return (
              
              <div className="enroll">
              <img className="img-fluid" src={enroll_01} />
              <img className="img-fluid" src={require('./img/enroll_02.gif')} />
              <img className="img-fluid" src={require('./img/enroll_03.gif')} />
              <Form className="enroll-form">
              <FormItem>
              {getFieldDecorator('name', {
            rules: [{
            required: true,
            message: '名称不能为空',
          }],
         })(
              <Input className="enroll-input" placeholder="商家名称" />
              )}
              </FormItem> 
               <FormItem>
                    {getFieldDecorator('shop', {
                 rules: [{
                 required: true,
                 message: '商品不能为空',
              
               }],
              })(
              <Input className="enroll-input" placeholder="商主营商品" />
              )}
              </FormItem>
              <FormItem>
               {getFieldDecorator('urse', {
            rules: [{
            required: true,
            message: '商品不能为空',
         
          }],
         })(
              <Input className="enroll-input" placeholder="联系人姓名" name="urse" />
              )}
              </FormItem>
                <FormItem>
                    {getFieldDecorator('nickname', {
                 rules: [{
                 required: true,
                  message:'只能输入数字',
                  pattern: /^[0-9]+$/
               }, {
                 len: 11,
                 message: '长度需11个字符',
               }],
              })(
              <Input className="enroll-input" placeholder="联系人电话" />
              )}
              </FormItem>
               <FormItem hasFeedback>
            {getFieldDecorator('email', {
              rules: [
                {
                  type: 'email',
                  message: '请输入正确的邮箱地址'
                },
                {
                  required: true,
                  message: '请输入邮箱地址'
                }
              ]
            })(
              <Input type="email" name="email" id="exampleEmail" className="enroll-input" placeholder="联系人邮箱" />
              )}
              </FormItem>
              <FormItem>
                    {getFieldDecorator('adress', {
                 rules: [{
                 required: true,
                 message: '地址不能为空',
              
               }],
              })(
              <Input className="enroll-input" placeholder="商家地址" />
               )}
              </FormItem>
             </Form>
             <Plus imgTitle="国家首页配图(1920*470)" 
             height="130px" 
              
             renderState={this.props.countryState == "add" ? "upload" : "init"}/>,
             <p style={{margin:15 ,}}>
            <Button color="primary" className="Enroll-bt"  >提交</Button>
            </p>
              </div>
              );
              }
}

  
export default Enroll= Form.create()(Enroll);