import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { Link } from 'react-router-dom';
import Index from './Index';
import Review from './Review';
import Information from './Information';
import Stand from './Stand';
import Contact from './Contact';
import Enroll from './Enroll';
import Traffic from './Traffic';
import Introduce from './Introduce';
import Activity from './Activity';
class Routerz extends Component {
  constructor(props) {
    super(props);

  }
   render() {
  
    return (
    
    <Router history={ browserHistory }>
                   <Route path="/" component={Index}/>
                   <Route path="/Review" component={Review} />
                   <Route path="/Information" component={Information} />
                   <Route path="/Stand" component={Stand} />
                   <Route path="/Contact" component={Contact} />
                   <Route path="/Index" component={Index} />
                   <Route path="/Enroll" component={Enroll} />
                   <Route path="/Traffic" component={Traffic} />
                   <Route path="/Introduce" component={Introduce} />
                   <Route path="/Activity" component={Activity} />
               </Router> 
    
    
    );
    }}
export default Routerz;