import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Index.css';
import { Container, Row, Col } from 'reactstrap';
class Review extends Component {
  constructor(props) {
    super(props);

  }


  render() {
    return (
    <div className="Review">
    <div className="details">
    <div className="detail">
      <div className="deta">
      <h3>历届正博会回顾</h3>
      <span className="despan">2018年4月18日9时28分</span><span className="despan1">阅读:20</span>,
      </div>
      <div className="article">
      中国.石家庄(正定)国际小商品博览会，以“融合商机、彰显魅力、协作发展、互动共赢”为宗旨，是集商贸、会展、交流与合作的国际性盛会，自2008年开始，至今已连续举办十届。先后被评为“2008年度中国最具成长性的品牌展会”“2008年度中国十大最 具发展潜力品牌展会’，2009年荣获“建国60周年全国知名品牌展会暨政府主导型展会百强”, 2011年获第八届“‘中国会展之星’品牌展会奖”“全国政府主 导型展会50强”, 2012年荣获“第九届中国会展之星大奖暨2011-2012年度中国十大品牌展览会”“十大优秀专业展会” ，2014年荣获“第十一届中国会展之星产业大奖暨中国十佳优秀特色展会”，2015年荣获了中国会展业年度大奖“2015年度中 国十佳优秀特色展会”，“2016- -2017年全国优秀展览 会奖”等殊荣。
      </div>
      <div className="article1">
     <div> <span className="artspan">首届正博会</span><span  className="artspan1"><i className="fa fa-circle-o" aria-hidden="true"></i>2008年4月26日</span></div>
      <div className="artimg"><img src={require('./img/rev1.png')} className="img-fluid"/></div>
      <div className="arttext">
       首届正博会于2008年4月26至28日日成功举办。首届正博会成果丰硕，共有来自27个国家或地区和国内20多个省、市、自治区的1638家企业参展、2800余家专业采购企业的7200余名采购商(其中有50个境外采购团组)前来参会,各展区参会参观的人数达到23万人次，总成交额达7.35亿元。</div>
<div className="arttext1">2009年，正博会在全国会展行业年会、第五届中国会展高峰论坛大会.上，2008中国.石家庄(正定)国际小商品博览会荣获“中国最具成长性的品牌展会”殊荣。
      </div>
      </div>
      
          <div className="article1">
           <div> <span className="artspan">首届正博会</span><span  className="artspan1"><i className="fa fa-circle-o" aria-hidden="true"></i>2008年4月26日</span></div>
            <div className="artimg"><img src={require('./img/rev1.png')} className="img-fluid"/></div>
            <div className="arttext">
             首届正博会于2008年4月26至28日日成功举办。首届正博会成果丰硕，共有来自27个国家或地区和国内20多个省、市、自治区的1638家企业参展、2800余家专业采购企业的7200余名采购商(其中有50个境外采购团组)前来参会,各展区参会参观的人数达到23万人次，总成交额达7.35亿元。</div>
      <div className="arttext1">2009年，正博会在全国会展行业年会、第五届中国会展高峰论坛大会.上，2008中国.石家庄(正定)国际小商品博览会荣获“中国最具成长性的品牌展会”殊荣。
            </div>
            </div>
      
      </div>
     </div>
     </div>
    );
  }
}

export default Review;
