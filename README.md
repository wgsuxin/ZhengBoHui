# ZhengBoHui

#### 介绍
正定国际小商品博览会 小程序 公众号个别页面

#### 软件架构
后端为基于 TP5.1 自建类 cms 系统，前端采用 taro 多端框架，默认适配小程序端，
项目技术栈主要为 PHP（ThinkPHP5.1）+ MySQL + React + Taro + 小程序API + TypeScrip + wxParse + SCSS


#### 安装教程

1. 克隆本项目代码，前端小程序代码目录为 myApp，进入该目录输入指令 npm install 进行安装即可
2. 公众号页面目录为 weix，安装步骤与小程序类似
3. 后端目录为 admin，默认服务器环境为 nginx 或 Apache，请自行准备服务器环境，新建站点并将根目录指向 admin 目录下的 public 目录，
   数据库文件为项目根目录下的 zbh.sql 文件，请自行导入，数据库名称默认为 zbh，名称及密码等可在 config/database.php 文件中进行修改

#### 使用说明

1. 后端直接运行站点配置的域名即可查看，默认账号密码为 admin / zhengbohui123，登录后可自行修改
2. 前端方面，小程序项目需要运行以下指令 npm run dev:weapp 启动开发模式，运行 npm run build:weapp 可构建生产代码，
   在微信开发工具导入 myAPP 目录即可进行调试查看，使用微信开发工具的其他流程与开发标准小程序是一样的
3. 需要最终在 H5 环境运行的项目，需要使用如下指令 npm run dev:h5 启动开发模式，运行 npm run build:h5 构建生产代码

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 其他